import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Text Style"),
        ),
        body: Center(
            child: Text("Dedi Triyadi",
                style: TextStyle(
                  fontSize: 50,
                  fontFamily: "Pacifico",
                ))),
      ),
    );
  }
}
